//
//  MessageBits.h
//  CalendarContentProvider
//
//  Created by Aditya Undirwadkar on 2/27/17.
//  Copyright © 2017 Aditya Undirwadkar. All rights reserved.
//

#ifndef MessageBits_h
#define MessageBits_h

#define INT32_SIZE 4
#define INT16_SIZE 2

// Convert a byte array to uint16_t
uint16_t getInt16(uint8_t* buff)
{
    uint16_t result = 0;
    for(int i=0; i<INT16_SIZE; ++i)
    {
        int x = (buff)[i] - '0';
        result = result * 10 + x;
    }
    return result;
}

// Convert a byte array to uint32_t with ascii values 
uint32_t getInt32(uint8_t* buff)
{
    uint32_t result = 0;
    for(int i=0; i<INT32_SIZE; ++i)
    {
        int x = (buff)[i] - '0';
        result = result * 10 + x;
    }
    return result;
}

// Bytes to int.
//uint32_t getInt32(uint8_t* buff)
//{
//    uint32_t result = (buff)[0] << 24;
//    result |= (buff)[1] << 16;
//    result |= (buff)[2] << 8;
//    result |= (buff)[3];
//    *buff += 4;
//    return result;
//}

#endif /* MessageBits_h */
