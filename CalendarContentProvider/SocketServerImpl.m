//
//  SocketServerImpl.c
//  CalendarContentProvider
//
//  Created by Aditya Undirwadkar on 2/26/17.
//  Copyright © 2017 Aditya Undirwadkar. All rights reserved.
//

#include "SocketServerImpl.h"
#include "MessageParser.h"

int setupTCPServer(struct tcpServer* pTCPServer){
    // Setup the TCP server.
    pTCPServer->listenfd = -1;
    pTCPServer->clientfd = -1;
    pTCPServer->listenfd = socket(AF_INET, SOCK_STREAM, 0);
    memset(&(pTCPServer->serv_addr), '0', sizeof(pTCPServer->serv_addr));
    pTCPServer->serv_addr.sin_family = AF_INET;
    pTCPServer->serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    pTCPServer->serv_addr.sin_port = htons(TCP_PORT);
    pTCPServer->_listenForConnections = 1;
    pTCPServer->continueReading = 1;
    return 1;
}

void startTCPServer(struct tcpServer* pTCPServer){
    
    bind(pTCPServer->listenfd, (struct sockaddr*)&(pTCPServer->serv_addr), sizeof(pTCPServer->serv_addr));
    listen(pTCPServer->listenfd, 10);
    
    NSLog(@"Waiting for connections...");
    NSMutableData* pReply = NULL;
    pTCPServer->clientfd = accept(pTCPServer->listenfd, (struct sockaddr*)NULL, NULL);
    while (pTCPServer->_listenForConnections != 0)
    {
        uint32_t payloadSize = 0;
        uint8_t header[PACKET_SIZE];
        recv(pTCPServer->clientfd , header, PACKET_SIZE , 0);
        payloadSize = getInt32(header);
        uint8_t payload[payloadSize];
        ssize_t bytesRead = recv(pTCPServer->clientfd , payload, payloadSize , 0);
        while (bytesRead < payloadSize) {
            bytesRead += recv(pTCPServer->clientfd , &payload[bytesRead], payloadSize-bytesRead , 0);
        }
        NSLog(@"## payload size: %zd data: %s", bytesRead, payload);
        //fwrite(payload, payloadSize, 1, stdout);
        // TODO: The below method should respond back with the reply for the query.
        pReply = ParseMessage(payload, payloadSize);
        write(pTCPServer->clientfd, [pReply bytes], [pReply length]);
    }
}

void stopTCPServer (struct tcpServer* pTCPServer){
    
    NSLog(@"Stop listening.");
    close(pTCPServer->listenfd);
}
