//
//  MessageParser.h
//  CalendarContentProvider
//
//  Created by Aditya Undirwadkar on 2/27/17.
//  Copyright © 2017 Aditya Undirwadkar. All rights reserved.
//

#ifndef MessageParser_h
#define MessageParser_h

#include <stdio.h>

#define QUERY     1
#define GETTYPE   2
#define INSERT    3
#define DELETE    4
#define UPDATE    5

//Common method.
NSMutableData* ParseMessage(uint8_t *pMessage, uint32_t payloadSize);

// Command ID based parsing.
NSMutableData* ParseQuery  (uint8_t *pMessage, uint32_t payloadSize);
NSMutableData* ParseInsert (uint8_t *pMessage, uint32_t payloadSize);
NSMutableData* ParseDelete (uint8_t *pMessage, uint32_t payloadSize);
NSMutableData* ParseUpdate (uint8_t *pMessage, uint32_t payloadSize);
NSMutableData* ParseGetType(uint8_t *pMessage, uint32_t payloadSize);

#endif /* MessageParser_h */
