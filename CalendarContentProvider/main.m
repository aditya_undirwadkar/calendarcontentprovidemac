//
//  main.m
//  CalendarContentProvider
//
//  Created by Aditya Undirwadkar on 2/25/17.
//  Copyright © 2017 Aditya Undirwadkar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SocketServerImpl.h"
#import "MessageResponder.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        struct tcpServer gTCPServer;
        
        if(setupTCPServer(&gTCPServer)) {
            startTCPServer(&gTCPServer);
            stopTCPServer (&gTCPServer);
        }
    }
    return 0;
}
