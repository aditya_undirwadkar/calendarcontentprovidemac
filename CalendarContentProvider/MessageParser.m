//
//  MessageParser.m
//  CalendarContentProvider
//
//  Created by Aditya Undirwadkar on 2/27/17.
//  Copyright © 2017 Aditya Undirwadkar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MessageParser.h"
#import "MessageBits.h"

 NSMutableData* ParseMessage(uint8_t *pMessage, uint32_t payloadSize)
{
    uint8_t* messagePtr = pMessage;
    uint32_t commandID = getInt32(messagePtr);
    messagePtr += INT32_SIZE;
    
    switch(commandID){
            
        case QUERY:
            
            return ParseQuery(messagePtr, payloadSize-INT32_SIZE);
            
            break;

        case GETTYPE:
            
            break;

        case INSERT:
            
            break;

        case DELETE:
            
            break;

        case UPDATE:
            
            break;

    }
    
    return NULL;
}
