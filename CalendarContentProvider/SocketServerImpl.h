//
//  SocketServerImpl.h
//  CalendarContentProvider
//
//  Created by Aditya Undirwadkar on 2/26/17.
//  Copyright © 2017 Aditya Undirwadkar. All rights reserved.
//

#ifndef SocketServerImpl_h
#define SocketServerImpl_h

#include <stdio.h>
#include <sys/types.h>
#include <netinet/in.h>
#import <Foundation/Foundation.h>

#define TCP_PORT 8008
#define PACKET_SIZE 4

struct tcpServer {
    int listenfd;                   // File descriptor for TCP server to listen the incoming client connections.
    int clientfd;                   // File desc for TCP client.
    struct sockaddr_in serv_addr;   // Holds the server's information.
    char* pBuffer;                  // Variable lenght buffer to hold the incoming request.
    int _listenForConnections;      // Flag to start listening the incoming connections.
    int continueReading;            // Flag to indicate whether the whole packet has been read or not.
};

int setupTCPServer(struct tcpServer* pTCPServer);
void startTCPServer(struct tcpServer* pTCPServer);
void stopTCPServer (struct tcpServer* pTCPServer);

#endif /* SocketServerImpl_h */
