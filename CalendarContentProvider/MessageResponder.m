//
//  MessageResponder.m
//  CalendarContentProvider
//
//  Created by Aditya Undirwadkar on 2/28/17.
//  Copyright © 2017 Aditya Undirwadkar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MessageResponder.h"


static inline void Set4BE(uint8_t* buf, uint32_t val) {
    *buf++ = (uint8_t)(val >> 24);
    *buf++ = (uint8_t)(val >> 16);
    *buf++ = (uint8_t)(val >> 8);
    *buf = (uint8_t)(val);
}


@implementation MessageResponder
-(MessageResponder *) initRows: (NSArray *)rowData {
    self = [super init];
    
    if(self) {
        self.tableRows = rowData;
    }
    
    return self;
}

-(void) printRows {
    for(NSArray *row in self.tableRows) {
        NSLog(@"Row.. ");
        for(NSString *item in row) {
            NSLog(@"%@", item);
        }
    }
}

-(NSMutableData *) serializeRows {
    
    NSMutableData* buffer       = [NSMutableData new];
    NSMutableData* pResponse    = [NSMutableData new];
    uint32_t numRows            = 0;          // Number of total rows.
    uint32_t sizeOfInt = sizeof(uint32_t);
    uint8_t tempSize[4];
    
    // Add the number of rows.
    // Add the number of rows the payload will contain.
    if(self.tableRows != NULL) {
        numRows = (int32_t)[self.tableRows count];
    }
    
    Set4BE(tempSize, numRows);
    [buffer appendBytes:&tempSize length:sizeOfInt];
    
        // Enumerate through the rowData and serialize each row.
        for(NSArray *row in self.tableRows) {
            for(NSString *item in row) {
                const char* pColumn = [item UTF8String];
                uint32_t columnSize = (uint32_t)strlen(pColumn);
                Set4BE(tempSize, columnSize);
                [buffer appendBytes:&tempSize length:sizeOfInt];
                [buffer appendBytes:pColumn length:columnSize];
            }
        }

    Set4BE(tempSize, (uint32_t)(buffer.length + sizeOfInt));    // The buffer will contain data plus the first four bytes to hold the size of the payload.
    [pResponse appendBytes:&tempSize length:sizeOfInt];         // Add the size of the packet to the response.
    [pResponse appendData:buffer];                              // Add the whole data to the response.
    
    // Return the response.
    return pResponse;
}

@end
