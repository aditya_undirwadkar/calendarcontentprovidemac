//
//  QueryParser.m
//  CalendarContentProvider
//
//  Created by Aditya Undirwadkar on 2/27/17.
//  Copyright © 2017 Aditya Undirwadkar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MessageParser.h"
#import "MessageResponder.h"


static int count = 0;

NSMutableData* ParseQuery(uint8_t *pMessage, uint32_t payloadSize)
{
    uint8_t* pQueryMessage = pMessage;
    
    // Copy the URI.
    uint32_t lenURI = getInt32(pQueryMessage);
    pQueryMessage += INT32_SIZE;
    if(lenURI > 0)
    {
        uint8_t pURI[lenURI+1];
        memcpy(pURI, pQueryMessage, lenURI);
        pURI[lenURI] = '\0';
        pQueryMessage += lenURI;
        printf("pURI: %s\n", pURI);
        
    }
    
    // Copy the Projection.
    uint32_t lenProjection = getInt32(pQueryMessage);
    pQueryMessage += INT32_SIZE;
    if(lenProjection > 0)
    {
        
        uint8_t pProjection[lenProjection+1];
        memcpy(pProjection, pQueryMessage, lenProjection);
        pProjection[lenProjection] = '\0';
        pQueryMessage += lenProjection;
        printf("pProjection: %s\n", pProjection);
       
    }
    
    if(pQueryMessage < pMessage+payloadSize)
    {
        
        // Copy the Selection.
        uint32_t lenSelection = getInt32(pQueryMessage);
        pQueryMessage += INT32_SIZE;
        uint8_t pSelection [lenSelection+1];
        memcpy(pSelection, pQueryMessage, lenSelection);
        pSelection[lenSelection] = '\0';
        pQueryMessage += lenSelection;
        printf("pSelection: %s\n", pSelection);
    }
    
    if(pQueryMessage < pMessage+payloadSize)
    {
        // Copy the SelectionArgs.
        uint32_t lenSelectionArgs = getInt32(pQueryMessage);
        pQueryMessage += INT32_SIZE;
        uint8_t pSelectionArgs[lenSelectionArgs+1] ;
        memcpy(pSelectionArgs, pQueryMessage, lenSelectionArgs);
        pSelectionArgs[lenSelectionArgs] = '\0';
        pQueryMessage += lenSelectionArgs;
        printf("pSelectionArgs: %s\n", pSelectionArgs);

    }
    
    if(pQueryMessage < pMessage+payloadSize)
    {
        // Copy the SelectionArgs.
        uint32_t lenSortOrder = getInt32(pQueryMessage);
        pQueryMessage += INT32_SIZE;
        uint8_t pSortOrder[lenSortOrder+1];
        memcpy(pSortOrder, pQueryMessage, lenSortOrder);
        pSortOrder[lenSortOrder] = '\0';
        pQueryMessage += lenSortOrder;
        printf("pSortOrder: %s\n", pSortOrder);
    }
    
    
    
    // This is where we will get the respons back from OS calendar in the form of NSArray.
    // Start serializing it here by calling the methods on the class QueryResponder.
    // This class will provide the rows in serialized manner to be able to send them back to
    // Android part.
    
    
    // Test code for nested NSArray as rows will be in this form.
    NSMutableData* pReply;
//    NSMutableData* pReply2;

    // As of now we will send just two packets.
    // 1st one will be calendar rows.
    if(count == 0)
    {
        NSArray  *obj1 = [[NSArray alloc] initWithObjects:@"1",nil];
        NSMutableArray * nsArray1 = [NSMutableArray new];
        [nsArray1 addObject:obj1];
        MessageResponder *msg1 = [[MessageResponder alloc] initRows: nsArray1];
        pReply = [msg1 serializeRows];
        obj1 = nil;
    }
    // 2nd one will be the events hardcoded as a response.
    else if(count == 1){
        NSArray  *obj3 = [[NSArray alloc]
                          initWithObjects:  @"Test Event",
                          @"San Bruno",
                          @"0",
                          @"6687707",
                          @"America/Los_Angeles",
                          @"15",
                          @"1487620800",
                          @"1487631600",
                          @"0",
                          @"2457811",
                          @"2457811",
                          @"720",
                          @"900",
                          @"0",
                          @"FREQ=NEVER",
                          @"",
                          @"1",
                          @"aditya@mechdome.com",
                          @"1",
                          @"0",
                          nil];
        
        
        NSArray  *obj2 = [[NSArray alloc]
                          initWithObjects:  @"Another Test Event",
                          @"Sunnyvale",
                          @"0",
                          @"16244235",
                          @"America/Los_Angeles",
                          @"15",
                          @"1487631600",
                          @"1487642400",
                          @"0",
                          @"2457813",
                          @"2457813",
                          @"800",
                          @"960",
                          @"0",
                          @"FREQ=NEVER",
                          @"",
                          @"1",
                          @"aditya@mechdome.com",
                          @"1",
                          @"0",
                          nil];
        
        
        NSMutableArray * nsArray2 = [NSMutableArray new];
        
        [nsArray2 addObject:obj3];
        [nsArray2 addObject:obj2];
        MessageResponder *msg2 = [[MessageResponder alloc] initRows: nsArray2];
        pReply = [msg2 serializeRows];
    }
    if(count == 0) {
        count++;
        return pReply;
    }
    return pReply;
}
