//
//  MessageResponder.h
//  CalendarContentProvider
//
//  Created by Aditya Undirwadkar on 2/28/17.
//  Copyright © 2017 Aditya Undirwadkar. All rights reserved.
//

#ifndef MessageResponder_h
#define MessageResponder_h

#import <Foundation/NSObject.h>

#define INT32_SIZE 4

@interface MessageResponder: NSObject

@property (nonatomic) NSArray *tableRows;


-(MessageResponder*) initRows: (NSArray *)tableRows;        // Initialize the member with incoming data.
-(void) printRows;                                          // Sample print row method to print out the data.
-(NSMutableData *) serializeRows;                           // Serialize the data so that single byte stream can send it to android.

@end
#endif /* MessageResponder_h */
